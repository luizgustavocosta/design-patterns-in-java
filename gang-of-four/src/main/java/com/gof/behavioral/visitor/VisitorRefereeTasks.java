package com.gof.behavioral.visitor;

/**
 * The interface Visitor referee tasks.
 */
public interface VisitorRefereeTasks {

    /**
     * Check teams.
     */
    void checkTeams();

    /**
     * Check date and time.
     */
    void checkDateAndTime();

    /**
     * Check players.
     */
    void checkPlayers();
}
